# Ganesh SSH Default Password - (In)Secure Shell

## Dificuldade: **Easy** 

## Descrição

O Charles estava consado de ser rastreado pela internet e ver anúncios
personalizados ([patos de borracha](https://youtu.be/uYOmtEcZ1lk)
principalmente), então decidiu comprar um RPi e configurar um dns
sinkhole com o [Pi-hole](https://pi-hole.net/) para se livrar, de uma
vez por todas, de telemetria não autorizada.

Assim que a RPi chegou, o Charles gravou o Raspberry OS e configurou
o Pi-hole. Simples e fácil. Porém, ele também queria poder acessar a
RPi pela internet e configurou um servidor SSH, redirecionando uma
porta no roteador.

## Hints

- O Charles esqueceu completamente da reunião sobre SSH e não estava
  usando chaves assimétricas para autenticação.

## Setup

Para construir o container basta usar o comando docker abaixo:

```
docker build -t nome-container .
```

Em seguida, é necessário rodar o contâiner que acabamos de construir,
lembrando que o servidor SSH está escutando na porta 3022.

```
docker run -dp 3022:3022 --restart unless-stopped nome-container:latest
```

## Flag

A flag é: `Ganesh{Ch4ng3_th3_d3f4u1t_p4ssw0rd}`.
