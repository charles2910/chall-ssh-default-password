FROM debian:latest

WORKDIR /etc

RUN useradd -ms /bin/bash pi -p '$1$HcUW97aZ$DeA3Tbn4skK3nWCc2bTTS.'
RUN apt update && apt upgrade -y && apt install -y openssh-server procps

RUN rm -f /etc/ssh/sshd_config
COPY sshd_config ssh/sshd_config
COPY startup_script.sh startup_script.sh

COPY pi/* /home/pi/

#CMD exec /bin/bash -c "trap : TERM INT; sleep infinity & wait"
CMD exec /bin/bash -c "service ssh start && ./startup_script.sh"
