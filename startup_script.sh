#! /usr/bin/env bash

while true; do
	sleep 300
	if ! /usr/bin/pgrep sshd; then
		service ssh restart
	fi
done
