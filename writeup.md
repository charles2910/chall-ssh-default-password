# Writeup chall (In)Secure Shell

O Charles com sua pressa para configurar o pi-hole, acabou esquecendo de
uma coisa bastante importante. Todas as raspberry's vem com um usuário e
senha padrão. Então, quando ele redirecionou uma porta do roteador para
acessar o SSH na RPi, basicamente toda a internet conseguiria acessar a
RPi.

O usuário padrão é `pi` e a senha é `raspberry`. Ao colocar as credenciais,
o usuário faz o login e há um arquivo com a flag em `/home/pi`.

Flag: `Ganesh{Ch4ng3_th3_d3f4u1t_p4ssw0rd}`
